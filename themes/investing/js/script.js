jQuery(document).ready(function($) {


    // Smooth Scroll

    var hashTagActive = "";
      $("#main-menu a").click(function (event) {
        $("#main-menu li").removeClass('active');
        $(this).parent().addClass('active'); //adding active class to clicked li.
        if ($(this.hash).offset() != null) { //fixes menu for non-front pages.
          if(hashTagActive != this.hash) { //this will prevent if the user click several times the same link to freeze the scroll.
            event.preventDefault();
            //calculate destination place
            var dest = 0;
            if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
              dest = $(document).height() - $(window).height();
            } else {
              dest = $(this.hash).offset().top;
            }
            //go to destination
            $('html,body').animate({
              scrollTop: dest
            }, 1000, 'swing');
            hashTagActive = this.hash;
          }
        }
      });


  // Animated Header
    var changeHeaderFirst = 100;
    var animatedHeader = function(){
      var scrollTop = $(window).scrollTop();

      if (scrollTop >= changeHeaderFirst) {
        $('#section-navigation').addClass('grow');
        $('#overlay').addClass('fade-in');
        $('#block-bean-logo').addClass('fade-out');
      }
      else {
        $('#section-navigation').removeClass('grow');
        $('#overlay').removeClass('fade-in');
        $('#block-bean-logo').removeClass('fade-out');
      }

    };

var $animation_elements = $('.animate');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position) && !$element.hasClass('in-view')) {
      $element.addClass('in-view');
    }
  });
}

$(window).scroll(function() {
    animatedHeader();
    check_if_in_view();
  });


});
