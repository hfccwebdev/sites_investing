Henry Ford College Base Theme 1.x for Drupal 7.x
----------------------------------------------------------

Project lead: Micah Webner <micah@hfcc.edu>

This theme was originally adapted from early versions of the Basic theme
developed by Raincity Studios found at http://drupal.org/project/basic

Since that time, these two themes have diverged widely. The 6.x-2.x and
7.x-1.x branches of this theme draws ideas from many different sources.

_____________________________________________________________________________

Installation

- Unpack the downloaded file and place the hfccbase folder in your Drupal
  installation under one of the following locations:

    * sites/all/themes
    * sites/all/custom/themes
    * sites/default/themes
    * sites/example.com/themes
    * profiles/customprofile/themes

- Log in as an administrator on your Drupal site and go to
  Administer > Site building > Themes (admin/build/themes) and enable the
  hfccbase theme.
